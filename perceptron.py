import sys
sys.path.append('.venv/lib/python3.11/site-packages')

from neural_network_from_scratch.neral_network import NeuralNetwork
class Perceptron(NeuralNetwork):
    """
    A class representing a Perceptron neural network.

    Attributes:
        learn_rate (float): The learning rate of the Perceptron.

    Methods:
        __init__(self, learn_rate): Initializes the Perceptron with the given learning rate.
        predict(self, value): Predicts the output for the given input value.
        train(self, value, expected): Trains the Perceptron with the given input value and expected output.
    """

    def __init__(self,numberInput ,learn_rate):
        super().__init__([numberInput,1,1], learn_rate)

    def predict(self, values):
        """
        Predicts the output for a given input value.

        Args:
            value: The input value to be predicted.

        Returns:
            The predicted output value.
        """
        return self.propagation(values, useSign=False)[0]

    def train(self, value, expected):
        """
        Trains the perceptron model using the given input value and expected output.

        Parameters:
            value (list): The input value for training.
            expected (list): The expected output value for training.

        Returns:
            None
        """
        self.retropropagation(value, [expected], useSign=False)

# nn = NeuralNetwork([64,2,1],0.1)
# print(nn.propagation([1. for i in range(64)]))
# for i in range(10000):
#     nn.retropropagation([1. for i in range(64)], [0.])
# print(nn.propagation([1. for i in range(64)]))

