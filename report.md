# Perceptron Lab report
### By Leonardo Mosqueda-Hernandez and Gaëtan Plisson

This project deals with a perceptron algorithm.
Has this project was one of the most interresting we had yet, we spent a lot of time going further than the original goal of the lab.
It was actually possible to use the scikit-learn algorithm to choose one algorythm, but we decided though to make our model, what I hope is good.

First we started with doing an alone perceptron, and then we overpassed the goal and made a full neral-network algorithm.
Though, this project is reaching a specific, hand-made neral-network module, that can be found here : https://gitlab.eurecom.fr/plisson/neural-network-from-scratch
However, it does not require to load it manually, as it is in requirements.txt ( pip install -r requirements.txt)

The main goal was to compare both neural network and perceptron in the same exercice, 
wich consits on recognizing 0s and 1s.

Using a multi-layer with multi perceptrons ( = neuron) each, we do not determine large enough difference with one perceptron alone. (26% error for perceptron between train and data, and 25% of error for neural network)

It may be because of the shape of the neural network or because we made an error in it
Then, any advices on making the neural network working better is welcome.

AI used : Github Copilot for code assistancy and docs generation.


